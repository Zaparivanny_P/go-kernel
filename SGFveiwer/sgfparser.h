#ifndef SGF_PARSER
#define SGF_PARSER
#include "sgfparserTree.h"

int sgfparseFillBufferFromFile(char* path);
int sgfparseDestroiBuffer();

int sgfparseRootProperties();
int sgfparseGameInfoProperties();
int sgfparseTimingProperties();
int sgfparseTree();
void sgfparseDectructor();
sgfparser_move* sgfparseGetMove(int move);
sgfparser_move* sgfparseGetNextMove();
sgfparser_move* sgfparseGetPrewMove();

typedef enum
{
	ACK						= 0,
	MEMORY_ALLOCATION_ERROR = 1,
	FAIL_NO_FOUND			= 2,
	NULL_POINTER_BUFFER		= 3,
}ErrorParser;

typedef enum
{
	ERROR_ODD_SQ_BRACKET = 1,
}errorCheckFile;

#endif
