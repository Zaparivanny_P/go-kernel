#ifndef SGF_PARSER_RULE
#define SGF_PARSER_RULE
#include <stdbool.h>

#define CHECK(a, b) if (!a) return b;
#define CHECKR(a, b) if (a) return b;
#define CHECKN(a, b) if (a);

typedef struct sgf_killinfo
{
	int dame;
	int stone;
}sgf_killinfo;

typedef struct sgfparseBoard
{
	int** board;
	unsigned short size_board;
}sgfparseBoard;

typedef enum
{
	STONE_BLACK	= 'X',
	STONE_WHITE = 'O',
}typeStone;

typedef enum
{
	ERROR_STONE_EXIST		= 1,
	ERROR_INFINITY_KO		= 2,
	ERROR_SUICIDAI_MOVE		= 3,
	ERROR_INPOSIBLE_MOVE	= 4,
	ERROR_MALLOC			= 5,

}ERROR_MOVE;

int sgfparserRuleConstructor(unsigned char board_size);
int sgfparserRuleDectructor();
int** sgfparserGetBoard();
ERROR_MOVE sgfparserAddMove(unsigned short x, unsigned short y, typeStone type);
ERROR_MOVE sgfparserHardAddMove(unsigned short x, unsigned short y, typeStone type);
ERROR_MOVE sgfparserAddNextMove(unsigned short x, unsigned short y);
bool sgfparseGetDame(unsigned short x, unsigned short y, sgf_killinfo *info_move);
void sgfparseKillGroup(unsigned short x, unsigned short y);

#endif
