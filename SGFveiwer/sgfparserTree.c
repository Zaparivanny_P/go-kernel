#include "sgfparserTree.h"
#include <stdio.h>
#include <stdlib.h>

void sgfparseConstructorStructSgfPparserDataBranch(sgfparser_branch* str)
{
	str->iterator = 0;
	str->lentgh_move = 0;
	str->connect_in = 0;
	str->size_node = 0;
	str->name_node = 0;

	str->connect_out = NULL;
	str->moves = NULL;
}

int sgfparseDestructorStructSgfPparserDataBranch(sgfparser_branch* str)
{
	if (str == NULL)
	{
		return 1;
	}
	if (str->moves != NULL) { free(str->moves); str->moves = NULL; };
	if (str->connect_out != NULL){ free(str->connect_out); str->connect_out = NULL; };

	return 0;
}

void sgfparsePrintBranch(sgfparser_branch* sgf_branch)
{
	sgf_branch->iterator = 0;
	while (sgf_branch->iterator < sgf_branch->lentgh_move)
	{
		printf("[LOG] x %i, y %i\n", (&sgf_branch->moves[sgf_branch->iterator])->x, (&sgf_branch->moves[sgf_branch->iterator])->y);
		sgf_branch->iterator++;
	}
}

bool sgfparseTreeConstructor(sgfparser_tree* str, int size)
{
	int i;
	str->size = size;
	str->size_node = 0;
	str->iterator = 0;
	str->index_branch = 0;
	str->sgf_branchs = (sgfparser_branch*)malloc(size * sizeof(sgfparser_branch));
	if (str->sgf_branchs == NULL)
	{
		return false;
	}

	for (i = 0; i < str->size; i++)
	{
		sgfparseConstructorStructSgfPparserDataBranch(&str->sgf_branchs[i]);					//init sgf_branch[i]
	}
	return true;
}

void sgfparseTreeDestructor(sgfparser_tree* str)
{
	int i;
	if (str == NULL)
		return;
	for (i = 0; i < str->size; i++)
	{
		sgfparseDestructorStructSgfPparserDataBranch(&str->sgf_branchs[i]);		//free	sgf_branch[i]
	}
	free(str->sgf_branchs);
}

int sgfparseTreeFindOutNode(sgfparser_tree* str_tree, short index)
{
	int i;
	for (i = 0; i < str_tree->size; i++)
	{
		if ((&str_tree->sgf_branchs[i])->name_node == index)
			return i;
	}
	return -1;
}

int sgfparseTreeCreateConnection(sgfparser_tree* str_tree)
{
	int j;
	short tmp, index;
	(&str_tree->sgf_branchs[0])->connect_in = -1;
	for (j = 1; j < str_tree->size; j++)					//rename connect
	{
		tmp =  (&str_tree->sgf_branchs[j])->connect_in;
		tmp = sgfparseTreeFindOutNode(str_tree, tmp);
		(&str_tree->sgf_branchs[j])->connect_in = tmp;
	}

	for (j = 1; j < str_tree->size; j++)					//size node
	{
		index = (&str_tree->sgf_branchs[j])->connect_in;
		(&str_tree->sgf_branchs[index])->size_node++;
	}

	for (j = 0; j < str_tree->size; j++)					//malloc list node
	{
		if ((&str_tree->sgf_branchs[j])->size_node != 0)
		{
			(&str_tree->sgf_branchs[j])->connect_out = (short*)malloc((&str_tree->sgf_branchs[j])->size_node * sizeof(short));
			if ((&str_tree->sgf_branchs[j])->connect_out == NULL)
				return -1;
		}
		(&str_tree->sgf_branchs[j])->iterator = 0;
	}

	for (j = 1; j < str_tree->size; j++)					//add address connect
	{
		index = (&str_tree->sgf_branchs[j])->connect_in;
		(&str_tree->sgf_branchs[index])->connect_out[(&str_tree->sgf_branchs[index])->iterator++] = j;
	}
	return 0;
}

void sgfparseTreeGetTrack(sgfparser_tree* str_tree, sgfparser_track* track, int indexBranch)
{
	int tmpIndex = indexBranch;
	int tmp = 0;
	track->lentgh = 0;
	while (indexBranch != 0)
	{
		indexBranch = (&str_tree->sgf_branchs[indexBranch])->connect_in;
		track->lentgh++;
	}
	track->track = (int*)malloc(track->lentgh * sizeof(int));
	if (track->track == NULL)
		return;
	
	indexBranch = tmpIndex;
	while (indexBranch != 0)
	{
		indexBranch = (&str_tree->sgf_branchs[indexBranch])->connect_in;
		track->track[tmp++] = indexBranch;
	}
}

