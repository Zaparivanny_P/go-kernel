#include "sgf_actionstack.h"

#include <stdio.h>
#include <stdlib.h>
//stackinfinity_stack *move_stack;

bool Sgfactionstack_init(sgfactions_stack* stack)
{
	return Stackinfinity_init(stack);
}


void Sgfactionstack_deinitmove(sgfactions_stack* stack)
{
	int i;
	for (i = 1; i < (SGFSTACK_SIZE_T - 1); i++)
	{
		if (stack->stack[i] != NULL)
		{
			printf("free move %p\n", stack->stack[i]);
			free(stack->stack[i]);
		}
	}
}

bool Sgfactionstack_deinit(sgfactions_stack* stack)
{
	unsigned int** tmp;

	while (stack->stack[SGFSTACK_SIZE_T - 1] != NULL)
	{
		stack->stack = (unsigned int**)stack->stack[SGFSTACK_SIZE_T - 1];
	}

	tmp = (unsigned int **)stack->stack[0];
	while (tmp != NULL)
	{
		Sgfactionstack_deinitmove(stack);
		printf("free %p\n", stack->stack);
		free(stack->stack);
		stack->stack = tmp;
		tmp = (unsigned int **)stack->stack[0];
	}
	Sgfactionstack_deinitmove(stack);
	printf("free %p\n", stack->stack);
	free((void*)stack->stack);
	stack->stack = NULL;
	return true;
}


bool Sgfactionstack_push(sgfactions_stack * stack, sgfmove_cmd cmd, char type, unsigned short x, unsigned short y)
{
	sgf_move_info* move = (sgf_move_info*)malloc(sizeof(sgf_move_info));
	if (move == NULL)
		return false;
	printf("malloc move %p\n", move);
	move->cmd = cmd;
	move->type = type;
	move->x = x;
	move->y = y;
	return Stackinfinity_push(stack, move);
	
}