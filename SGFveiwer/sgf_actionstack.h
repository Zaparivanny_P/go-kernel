#ifndef SGF_ACTION_STACK
#define SGF_ACTION_STACK

#include "stack_infinity.h"
#include "stdbool.h"


typedef enum sgfmove_cmd
{
	ADD_MOVE = 1,
	REMOVE_MOVE = 2,

}sgfmove_cmd;

typedef struct sgf_move_info
{
	sgfmove_cmd cmd;
	char type;
	unsigned short x;
	unsigned short y;
}sgf_move_info;

typedef stackinfinity_stack sgfactions_stack;
/*
{
stackinfinity_stack move_stack;
}sgfmovestack;*/


bool Sgfactionstack_init(sgfactions_stack* stack);
bool Sgfactionstack_deinit(sgfactions_stack* stack);

bool Sgfactionstack_push(sgfactions_stack * stack, sgfmove_cmd cmd, char type, unsigned short x, unsigned short y);
bool Sgfactionstack_pop(sgfactions_stack * stack, sgf_move_info *data);

bool Sgfactionstack_goto(sgfactions_stack * stack, int pos);
bool Sgfactionstack_end(sgfactions_stack * stack);

#endif
