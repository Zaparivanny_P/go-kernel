#ifndef STACK_INFINITY
#define STACK_INFINITY

#include <stdbool.h>
#define SGFSTACK_SIZE 10
#define SGFSTACK_SIZE_T (SGFSTACK_SIZE + 2)

typedef struct stackinfinity_stack
{
	unsigned int** stack;
	int iterator;
	int size;
	int abs_iterator;
}stackinfinity_stack;

bool Stackinfinity_init(stackinfinity_stack * stack);
bool Stackinfinity_deinit(stackinfinity_stack * stack);

bool Stackinfinity_pop(stackinfinity_stack * stack, void *data);
bool Stackinfinity_push(stackinfinity_stack * stack, void *data);

bool Stackinfinity_goto(stackinfinity_stack * stack, int pos);
bool Stackinfinity_end(stackinfinity_stack * stack);

#endif