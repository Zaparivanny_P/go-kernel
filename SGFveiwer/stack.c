#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

bool init_stack(stack *str, unsigned int size)
{
	str->stack_x = malloc(size * sizeof(unsigned int));
	if (str->stack_x == NULL)
	{
		return false;
	}
	str->stack_y = malloc(size * sizeof(unsigned int));
	if (str->stack_y == NULL)
	{
		return false;
	}
	str->Iterator = 0;
	str->size= size;
	return true;
}

void deinit_stack(stack *str)
{
	if (str->stack_x != NULL)
	{
		free(str->stack_x);
		str->stack_x = NULL;
	}
	if (str->stack_y != NULL)
	{
		free(str->stack_x);
		str->stack_y = NULL;
	}
}

bool stack_pop(stack *str, unsigned int *x, unsigned int *y)
{
	if (str->Iterator > 0)
	{
		str->Iterator--;
		*x = str->stack_x[str->Iterator];
		*y = str->stack_y[str->Iterator];
		//printf("POP %i %i\n", *x, *y);
		return true;
	}
	return false;
}
bool stack_push(stack *str, unsigned int x, unsigned int y)
{
	if (str->Iterator < str->size)
	{
		str->stack_x[str->Iterator] = x;
		str->stack_y[str->Iterator] = y;
		str->Iterator++;
		//printf("PUSH %i %i\n", x, y);
		return true;
	}
	return false;
}

void emptyStack(stack *str)
{
	str->Iterator = 0;
}