#include "stack_infinity.h"
#include <stdio.h>
#include <stdlib.h>

bool Stackinfinity_init(stackinfinity_stack * stack)
{
	stack->stack = (unsigned int**)calloc(SGFSTACK_SIZE_T, sizeof(unsigned int));
	printf("malloc %p\n", stack->stack);
	if (stack->stack == NULL)
	{
		return false;
	}
	stack->iterator = 0;
	stack->stack[SGFSTACK_SIZE_T - 1] = NULL;
	stack->stack[0] = NULL;
	stack->size = 0;
	stack->abs_iterator = 0;
	return true;
}

bool Stackinfinity_deinit(stackinfinity_stack * stack)
{
	unsigned int** tmp;
	while (stack->stack[SGFSTACK_SIZE_T - 1] != NULL)
	{
		stack->stack = (unsigned int**)stack->stack[SGFSTACK_SIZE_T - 1];
	}

	tmp = (unsigned int **)stack->stack[0];
	while (tmp != NULL)
	{
		printf("free %p\n", stack->stack);
		free(stack->stack);
		stack->stack = tmp;
		tmp = (unsigned int **)stack->stack[0];
	}
	printf("free %p\n", stack->stack);
	free((void*)stack->stack);
	stack->stack = NULL;
	return true;
}

bool Stackinfinity_push(stackinfinity_stack * stack, unsigned int *data)
{
	unsigned int ** ols_stack;
	stack->iterator++;
	stack->abs_iterator++;
	if (stack->abs_iterator >= stack->size)
	{
		stack->size = stack->abs_iterator;
	}
	//printf("stack iterator %i\n", stack->abs_iterator);
	if (data != NULL)
	{
		stack->stack[stack->iterator] = data;
	}

	if (stack->iterator == SGFSTACK_SIZE)
	{

		if (stack->stack[SGFSTACK_SIZE_T - 1] == NULL)
		{
			ols_stack = stack->stack;
			stack->stack = (unsigned int**)calloc(SGFSTACK_SIZE_T, sizeof(unsigned int));
			printf("malloc %p\n", stack->stack);
			if (stack->stack == NULL)
			{
				return false;
			}
			stack->stack[SGFSTACK_SIZE_T - 1] = NULL;
			ols_stack[SGFSTACK_SIZE_T - 1] = (unsigned int*)stack->stack;
			stack->iterator = 0;
			stack->stack[0] = (unsigned int*)ols_stack;
			return true;
		}
		stack->stack = (unsigned int**)stack->stack[SGFSTACK_SIZE_T - 1];
		stack->iterator = 0;
	}
	return true;
}

bool Stackinfinity_pop(stackinfinity_stack * stack, void *data)
{
	stack->iterator--;
	stack->abs_iterator--;
	if (data != NULL)
		data = stack->stack[stack->iterator];
	//printf("stack iterator %i\n", stack->abs_iterator);
	if (stack->iterator == 1)
	{
		if (stack->stack[0] == NULL)
		{
			stack->iterator++;
			stack->abs_iterator++;
			return false;
		}
		stack->stack = (unsigned int**)stack->stack[0];
		stack->iterator = SGFSTACK_SIZE_T - 1;
	}
	return true;
}


bool Stackinfinity_goto(stackinfinity_stack * stack, int pos)
{
	if (pos > stack->size)
		return false;

	if (pos < stack->abs_iterator)
	{
		while (pos < stack->abs_iterator)
		{
			if (!Stackinfinity_pop(stack, NULL))
				return false;
		}
	}
	else if (pos > stack->abs_iterator)
	{
		while (pos > stack->abs_iterator)
		{
			if (!Stackinfinity_push(stack, NULL))
				return false;
		}
	}
	return true;
}

bool Stackinfinity_end(stackinfinity_stack * stack)
{
	return Stackinfinity_goto(stack, stack->size);
}