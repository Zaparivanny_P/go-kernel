#ifndef SGF_MOVE_STACK_H
#define SGF_MOVE_STACK_H

#include <stdbool.h>
#include "stack_infinity.h"

typedef stackinfinity_stack sgf_stack_move;


bool Sgfmove_init();
bool Sgfmove_deinit();

bool Sgfmove_addAction();
bool Sgfmove_addMove();
bool Sgfmove_undoMove();



#endif 