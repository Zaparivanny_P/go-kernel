#include "sgfparser.h"

#include "sgf.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

char *_parse_buffer = NULL;
int size_parse_buffer = 0;
s_sgf_root_properties		sgf_rp;
s_sgf_game_info_properties	sgf_gip;
s_sgf_timing_properties		sgf_tp;

sgfparser_tree *sgf_tree = NULL;
sgfparser_track sgf_track;

int sgfparseInit()
{
	sgf_rp.AP = NULL;
	sgf_rp.CA = NULL;
	sgf_rp.FF = 0;
	sgf_rp.GM = 0;
	sgf_rp.ST = 0;
	sgf_rp.SZ = 0;

	sgf_tp.BL = 0;
	sgf_tp.OB = 0;
	sgf_tp.OW = 0;
	sgf_tp.WL = 0;

	sgf_track.track = NULL;
	return 0;
}

int sgfparseFillBufferFromFile(char* path)
{	
	int i;
	sgfparseInit();
	FILE * sgfFile;
	sgfFile = fopen(path, "r");
	if (sgfFile == NULL)
	{
		return FAIL_NO_FOUND;
	}

	fseek(sgfFile, 0, SEEK_END);
	size_parse_buffer = ftell(sgfFile);

	printf("size sgf file: %i\n", size_parse_buffer);
	_parse_buffer = (char*)malloc(size_parse_buffer * sizeof(char));

	if (_parse_buffer == NULL)
	{
		return MEMORY_ALLOCATION_ERROR;
	}

	fseek(sgfFile, 0, SEEK_SET);
	for (i = 0; i < size_parse_buffer; i++)
	{
		_parse_buffer[i] = (char)fgetc(sgfFile);
		//printf("%c", _parse_buffer[i]);
	}

	
	fclose(sgfFile);
	
	return 0;
}

int sgfparseDestroiBuffer()
{
	if (_parse_buffer == NULL)
	{
		return NULL_POINTER_BUFFER;
	}
	free(_parse_buffer);
	_parse_buffer = NULL;
	
	if (sgf_rp.AP != NULL)	//TODO kostil
	{
		free(sgf_rp.AP);
		sgf_rp.AP = NULL;
	}
	
	if (sgf_rp.CA != NULL)	//TODO kostil
	{
		free(sgf_rp.CA);
		sgf_rp.CA = NULL;
	}
	return 0;
}
// �������� �� ���������, �� ���� ������
int sgrparserGetIntParaneter(const char* nameParam, unsigned char* param)
{
	char* name_p;
	char * p_char;
	if (nameParam == NULL)
		return 4;
	name_p = (char*)malloc(sizeof(nameParam) + 1);
	if (name_p == NULL)
		return 1;
	if (memcpy(name_p, nameParam, sizeof(nameParam)) == NULL)
	{
		printf("[ERROR] Memory allocation error in 'sgrparserGetParaneter'\n");
		return 2;
	}
	name_p = strcat(name_p, "[");
	
	p_char = strstr(_parse_buffer, name_p);
	free(name_p);
	if (p_char == NULL)
	{
		printf("[ERROR] Parametr %s no found\n", nameParam);
		return 1;
	}	
	p_char += 3;
	*param = 0;
	while (*(p_char) != ']')
	{
		*param *= 10;
		*param += *(p_char++) - 48;
	}
	return 0;
}

int sgrparserGetStrParaneter(const char* nameParam, char** dest)
{
	char* name_p;
	int count_n = 0;
	char tmp_dest[50];
	const char * p_char;

	name_p = (char*)malloc(sizeof(nameParam) + 1);
	if (name_p == NULL)
		return 2;
	if (memcpy(name_p, nameParam, sizeof(nameParam)) == NULL)
	{
		printf("[ERROR] Memory allocation error in 'sgrparserGetStrParaneter'\n");
	}
	name_p = strcat(name_p, "[");
	//if (name_p == NULL)
	//	return 3;
	
	p_char = strstr(_parse_buffer, name_p);
	if (p_char == NULL)
	{
		//printf("[ERROR] Parametr %s no found \n", nameParam);
		return 1;
	}
	free(name_p);
	p_char += 3;
	while (*(p_char) != ']')
	{
		tmp_dest[count_n++] = *p_char++;
	}
	tmp_dest[count_n++] = '\0';
	*dest = (char*)malloc((count_n)* sizeof(char));
	memcpy(*dest, tmp_dest, count_n);
	printf("[DEBUG] Parametr %s\n", *dest);
	return 0;
}

int sgfparseRootProperties()
{
	if (_parse_buffer == NULL)
	{
		return NULL_POINTER_BUFFER;
	}

	sgrparserGetIntParaneter("SZ", &sgf_rp.SZ);
	printf("[INFO] Size board: %i\n", sgf_rp.SZ);

	sgrparserGetIntParaneter("FF", &sgf_rp.FF);
	printf("[INFO] Format file: %i\n", sgf_rp.FF);

	sgrparserGetIntParaneter("GM", &sgf_rp.GM);
	printf("[INFO] GAME: %i\n", sgf_rp.GM);

	sgrparserGetIntParaneter("ST", &sgf_rp.ST);
	printf("[INFO] ST: %i\n", sgf_rp.ST);

	sgrparserGetStrParaneter("AP", &sgf_rp.AP);
	sgrparserGetStrParaneter("CA", &sgf_rp.CA);

	return 0;
}

int sgfparseGameInfoProperties()
{
	if (_parse_buffer == NULL)
	{
		return NULL_POINTER_BUFFER;
	}

	sgrparserGetStrParaneter("AN", &sgf_gip.AN);
	sgrparserGetStrParaneter("BR", &sgf_gip.BR);
	sgrparserGetStrParaneter("BT", &sgf_gip.BT);
	sgrparserGetStrParaneter("CP", &sgf_gip.CP);
	sgrparserGetStrParaneter("DT", &sgf_gip.DT);
	sgrparserGetStrParaneter("EV", &sgf_gip.EV);
	sgrparserGetStrParaneter("GC", &sgf_gip.GC);
	sgrparserGetStrParaneter("GN", &sgf_gip.GN);
	sgrparserGetStrParaneter("ON", &sgf_gip.ON);
	sgrparserGetStrParaneter("OT", &sgf_gip.OT);
	sgrparserGetStrParaneter("PB", &sgf_gip.PB);
	sgrparserGetStrParaneter("PC", &sgf_gip.PC);
	sgrparserGetStrParaneter("PW", &sgf_gip.PW);
	sgrparserGetStrParaneter("RE", &sgf_gip.RE);
	sgrparserGetStrParaneter("RO", &sgf_gip.RO);
	sgrparserGetStrParaneter("RU", &sgf_gip.RU);
	sgrparserGetStrParaneter("SO", &sgf_gip.SO);
	sgrparserGetStrParaneter("TM", &sgf_gip.TM);
	sgrparserGetStrParaneter("US", &sgf_gip.US);
	sgrparserGetStrParaneter("WR", &sgf_gip.WR);
	sgrparserGetStrParaneter("WT", &sgf_gip.WT);

	return 0;
}

int sgfparseDestructGameInfoProperties()
{
	/*
	if (sgf_gip.AN != NULL){ free(sgf_gip.AN); };
	if (sgf_gip.BR != NULL){ free(sgf_gip.BR); };
	if (sgf_gip.BT != NULL){ free(sgf_gip.BT); };
	if (sgf_gip.CP != NULL){ free(sgf_gip.CP); };
	if (sgf_gip.DT != NULL){ free(sgf_gip.DT); };
	if (sgf_gip.EV != NULL){ free(sgf_gip.EV); };
	if (sgf_gip.GC != NULL){ free(sgf_gip.GC); };
	if (sgf_gip.GN != NULL){ free(sgf_gip.GN); };
	if (sgf_gip.ON != NULL){ free(sgf_gip.ON); };
	if (sgf_gip.OT != NULL){ free(sgf_gip.OT); };
	if (sgf_gip.PB != NULL){ free(sgf_gip.PB); };
	if (sgf_gip.PC != NULL){ free(sgf_gip.PC); };
	if (sgf_gip.PW != NULL){ free(sgf_gip.PW); };
	if (sgf_gip.RE != NULL){ free(sgf_gip.RE); };
	if (sgf_gip.RO != NULL){ free(sgf_gip.RO); };
	if (sgf_gip.RU != NULL){ free(sgf_gip.RU); };
	if (sgf_gip.SO != NULL){ free(sgf_gip.SO); };
	if (sgf_gip.TM != NULL){ free(sgf_gip.TM); };
	if (sgf_gip.US != NULL){ free(sgf_gip.US); };
	if (sgf_gip.WR != NULL){ free(sgf_gip.WR); };
	if (sgf_gip.WT != NULL){ free(sgf_gip.WT); };*/
	return 0;
}

/************************************************************************************/

int sgfparseTreeGetNextIndexOfChar(int startIndex, const char ch)
{
	char* tmp_ptr = _parse_buffer + startIndex;
	while (*tmp_ptr++ != ch)
	{
		if (++startIndex > size_parse_buffer)
		{
			return -1;
		}
	}
	return startIndex;
}

int sgfparseTreeGetNextIndexOfOpenBracket(int startIndex)
{
	return sgfparseTreeGetNextIndexOfChar(startIndex, '(');
}

int sgfparseTreeGetNextIndexOfCloseBracket(int startIndex)
{
	return sgfparseTreeGetNextIndexOfChar(startIndex, ')');
}

/************************************************************************************/

bool sgfparseCheckOutTag(index)
{
	char* tmp_ptr = _parse_buffer;
	int _index = 0;
	bool state = true;
	while (_index++ < index)
	{
		tmp_ptr++;
		if (*tmp_ptr == '[')
		{
			state = false;
			continue;
		}
		if (*tmp_ptr == ']')
		{
			if (*(tmp_ptr - 1) == '\\')
			{
				continue;
			}
			state = true;
		}
	}

	return state;
}

bool sgfparseCheckCubeBracket(char * ptr)
{
	if (*ptr == ']')
	{
		if (*(ptr - 1) == '\\')
		{
			return false;
		}
		return true;
	}
	return false;
}

int sgfparseGoOutTag(index)
{
	char* tmp_ptr = _parse_buffer + index;
	while (1)
	{
		tmp_ptr++;
		if (*tmp_ptr == ']')
		{
			if (*(tmp_ptr - 1) == '\\')
			{
				continue;
			}
			break;
		}
		if (++index > size_parse_buffer)
		{
			return -1;
		}
	}
	return index;
}


int sgfparseTreeGetNextIndexOfTag(int startIndex, const char* ch1)
{
	int tmp_index = 0;

	if (!sgfparseCheckOutTag(startIndex))
	{
		tmp_index = sgfparseGoOutTag(startIndex);
	}
	return startIndex;
}

bool sgfparseCheckTag(char *buff, int amount_value)
{
	//if ((*(buff - 1) == ';') || (*(buff - 1) == ']') || (*(buff - 1) == '\n'))
	//{
	if ((*(buff + 1) == '[') && (*(buff + 2 + amount_value) == ']'))
	{
		return true;
	}
	
	//}
	else
	{
		printf("[sgfparseCheckTag] Error tag ->%c%c%c%c%c%c<- falure \n", *(buff - 1), *buff, *(buff + 1), *(buff + 2), *(buff + 3), *(buff + 4));
		return false;
	}
	printf("[sgfparseCheckTag] Check tag [%c] falure: '", *buff);
	printf("%c", *(buff + 1));
	printf("%c", *(buff + 2));
	printf("%c", *(buff + 3));
	printf("%c'\n", *(buff + 4));
	return false;
}


int sgfparseTreeGetLentghBranch(int index)
{
	char *tmp_ptr = _parse_buffer + index;
	int size_branch = 0;
	bool state_bracket = true;
	if (*(tmp_ptr) != '(')
	{
		printf("[sgfparseTreeGetLentghBranch] Pointer error\n");
		return -1;
	}
	while (1)
	{
		if (++tmp_ptr > size_parse_buffer + _parse_buffer)
		{
			return -1;
		}

		if (*tmp_ptr == '[')
		{
			//printf("[sgfparseTreeGetLentghBranch] IN BRACKET %c%c%c%c\n", *(tmp_ptr - 1), *(tmp_ptr + 0), *(tmp_ptr + 1), *(tmp_ptr + 2));
			while (!sgfparseCheckCubeBracket(++tmp_ptr));
			//printf("[sgfparseTreeGetLentghBranch] OUT BRACKET %c%c%c%c\n", *(tmp_ptr - 1), *(tmp_ptr + 0), *(tmp_ptr + 1), *(tmp_ptr + 2));			
			continue;
		}

		if ((*tmp_ptr == '(') || (*tmp_ptr == ')'))
		{
			break;
		}

		if ((*tmp_ptr == 'W') || (*tmp_ptr == 'B'))
		{
			if (*(tmp_ptr - 1) != ';')
			{
				continue;
			}
			
			if (!sgfparseCheckTag(tmp_ptr, 2))
			{
				printf("[sgfparseTreeGetLentghBranch] Error tag %c\n", *tmp_ptr);
				continue;
			}
			tmp_ptr += 4;
			size_branch++;
		}
	}
	return size_branch;
}



int sgfparseBranchOfTree(int index, sgfparser_branch* sgf_brn)
{
	char *tmp_ptr = _parse_buffer + index;
	bool state = true;
	unsigned int len_branch;
	char* tmpindex = tmp_ptr;
	if (_parse_buffer == NULL)
		return -1;
	while (*tmp_ptr++ != '(')
	{
		index++;
		if (tmp_ptr > _parse_buffer + size_parse_buffer)
		{
			return -1;
		}
	} 
	//printf("[sgfparseBranchOfTree] Start char %c\n", *(tmp_ptr - 1));
	len_branch = sgfparseTreeGetLentghBranch(index);
	if (len_branch == -1)
	{
		return -1;
	}
	sgf_brn->lentgh_move = len_branch;
	sgf_brn->moves = malloc(len_branch * sizeof(sgfparser_move));
	if (sgf_brn->moves == NULL)
	{
		return -1;
	}
	sgf_brn->iterator = 0;
	
	while (state)
	{
		tmp_ptr++;
		if (*tmp_ptr == '[')
		{
			while (!sgfparseCheckCubeBracket(++tmp_ptr));
			continue;
		}
		if ((*tmp_ptr == 'W') || (*tmp_ptr == 'B'))
		{
			if (!sgfparseCheckTag(tmp_ptr, 2))
			{
				continue;
			}
			(&sgf_brn->moves[sgf_brn->iterator])->x = *(tmp_ptr + 2) - 96;
			(&sgf_brn->moves[sgf_brn->iterator])->y = *(tmp_ptr + 3) - 96;
			if ((++sgf_brn->iterator) > len_branch)
			{
				printf("[sgfparseBranchOfTree] Out of size array sgf_brn\n");
				return -1;
			}
		}

		switch (*tmp_ptr)
		{
		case 'W':
			(&sgf_brn->moves[sgf_brn->iterator - 1])->typeStone = 1;
			break;
		case 'B':
			(&sgf_brn->moves[sgf_brn->iterator - 1])->typeStone = 0;
			break;
		case '(':
			sgf_brn->typeBranch = 0;
			state = false;
			break;
		case ')':
			sgf_brn->typeBranch = 1;
			state = false;
			break;
		default:
			break;
		}
	}
	return tmp_ptr - tmpindex + index - 1;
}

int sgfparseTreeCheckFormat(int* a_node)
{
	int amount_node = 0;
	bool entryInSqBracket = false;
	char* tmp_ptr;

	for (tmp_ptr = _parse_buffer; tmp_ptr < (size_parse_buffer + _parse_buffer); tmp_ptr++)
	{
		if (entryInSqBracket)
		{
			while (sgfparseCheckCubeBracket(tmp_ptr++))
			{
				if (tmp_ptr > size_parse_buffer + _parse_buffer)	//TODO >= or > ?
					return ERROR_ODD_SQ_BRACKET;
			}
			entryInSqBracket = true;
		}

		if (*tmp_ptr == '(')
		{
			amount_node++;
		}
		
	}
	*a_node = amount_node;
	return 0;
}

int sgfparseTree()
{
	int i = 0;
	short count_node = -1;
	short prew_count_node = 0;
	short num_node = 0;
	int index = 0;
	int a_node;

	printf("[PARSE] Start\n");
	if (_parse_buffer == NULL)
	{
		return NULL_POINTER_BUFFER;
	}

	
	sgfparseTreeCheckFormat(&a_node);
	
	sgf_tree = malloc(sizeof(sgfparser_tree));
	if (sgf_tree == NULL)
		return 1;
	if (!sgfparseTreeConstructor(sgf_tree, a_node))
		return -1;

	
	for (i = 0; i < sgf_tree->size; i++)
	{
		
		switch (*(_parse_buffer + index))
		{
		case '(':
			count_node++;
			break;
		case ')':
			count_node--;
			while (true)
			{
				index++;
				if (*(_parse_buffer + index) == '(')
				{
					count_node++;
					break;
				}
				if (*(_parse_buffer + index) == ')')
				{
					count_node--;
					continue;
				}
			}
			break;
		default:
			printf("[LOG] Error start char  branch [%c]\n", *(_parse_buffer + index));
			sgf_tree->sgf_branchs[i].connect_in = 0;
			break;
		}
		if (prew_count_node == count_node - 1)
		{
			num_node++;
			sgf_tree->sgf_branchs[i].connect_in = num_node;
			if (i != 0)
				sgf_tree->sgf_branchs[i - 1].name_node = num_node;
		}
		else if (prew_count_node > count_node)
		{
			sgf_tree->sgf_branchs[i].connect_in = count_node;
		}
		else
		{
			sgf_tree->sgf_branchs[i].connect_in = num_node;
		}
		prew_count_node = count_node;
		

		index = sgfparseBranchOfTree(index, &sgf_tree->sgf_branchs[i]) + 1;
		if (index == -1)
		{
			break;
		}
		

		//printf("[LOG] New branch %i, type %i, index %i, connect to %i \n", i, sgf_tree.sgf_branchs[i].typeBranch, index, sgf_tree.sgf_branchs[i].connect_in);
		sgfparsePrintBranch(&sgf_tree->sgf_branchs[i]);
	}
	sgf_tree->size_node = num_node;
	if (sgfparseTreeCreateConnection(sgf_tree) == -1)
		return -1;
	
	for (i = 0; i < sgf_tree->size; i++)
	{
		printf("[LOG] Branch %i, node %i, connect to %i\n", i, sgf_tree->sgf_branchs[i].name_node, sgf_tree->sgf_branchs[i].connect_in);
	}
	
	return 0;
}

void sgfparseDectructor()
{
	if (sgf_track.track != NULL) { free(sgf_track.track); sgf_track.track = NULL; };
	if (sgf_tree != NULL) { free(sgf_tree); sgf_tree = NULL; };
	sgfparseTreeDestructor(sgf_tree);
	sgfparseDestructGameInfoProperties();
	sgfparseDestroiBuffer();
}


sgfparser_move* sgfparseGetMove(int move)
{
	short indexBranch = 0;
	int tmp_move = move;

	if (sgf_tree == NULL)
	{
		return NULL;
	}
	move--;
	
	while (1)
	{
		move = tmp_move;
		tmp_move -= (&sgf_tree->sgf_branchs[indexBranch])->lentgh_move;
		if (tmp_move < 0)
		{
			return &((&sgf_tree->sgf_branchs[indexBranch])->moves[move]);
		}

		if ((&sgf_tree->sgf_branchs[indexBranch])->connect_out == NULL)
		{
			printf("[sgfparseGetMove] Error move out\n");
			break;
		}
			
		indexBranch = (&sgf_tree->sgf_branchs[indexBranch])->connect_out[0];
		if ((indexBranch == -1) || (indexBranch == 0))
		{
			break;
		}
	}
	return NULL;
}


sgfparser_move* sgfparseGetNextMove()
{
	int tmp_move = sgf_tree->iterator++;
	int move = tmp_move;

	tmp_move -= (&sgf_tree->sgf_branchs[sgf_tree->index_branch])->lentgh_move;
	if (tmp_move < 0)
	{
		return &((&sgf_tree->sgf_branchs[sgf_tree->index_branch])->moves[move]);
	}

	if ((&sgf_tree->sgf_branchs[sgf_tree->index_branch])->connect_out == NULL)
	{
		printf("[sgfparseGetNextMove] Error move out\n");
		sgf_tree->iterator--;
		return NULL;
	}
	sgf_tree->index_branch = (&sgf_tree->sgf_branchs[sgf_tree->index_branch])->connect_out[0];
	sgf_tree->iterator = tmp_move + 1;
	return &((&sgf_tree->sgf_branchs[sgf_tree->index_branch])->moves[tmp_move]);
}

sgfparser_move* sgfparseGetPrewMove()
{
	int tmp;
	if (--sgf_tree->iterator == -1)
	{
		sgf_tree->iterator++;
		tmp = (&sgf_tree->sgf_branchs[sgf_tree->index_branch])->connect_in;
		if (tmp == -1)
		{
			printf("[sgfparseGetPrewMove] Move out\n");
			return NULL;
		}
		sgf_tree->index_branch = (&sgf_tree->sgf_branchs[sgf_tree->index_branch])->connect_in;
		sgf_tree->iterator = (&sgf_tree->sgf_branchs[sgf_tree->index_branch])->lentgh_move - 1;
	}
	
	return &((&sgf_tree->sgf_branchs[sgf_tree->index_branch])->moves[sgf_tree->iterator]);
}
