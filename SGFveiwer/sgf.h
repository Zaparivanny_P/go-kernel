#include "sgfparser.h"
#include "sgfparserLogger.h"
#include "sgfpraserRule.h"

typedef struct sgf_root_properties
{
	char* AP ;	//Provides the name and version number of the application
	char* CA;	//Provides the used charset for SimpleText and Text type.
	unsigned char FF; 	//File format: version of SGF specification governing this SGF file. 
	unsigned char GM;	//Game: type of game represented by this SGF file. A property value of 1 refers to Go.
	unsigned char ST;	//Defines how variations should be shown;
	unsigned char SZ;	//Defines the size of the board.
}s_sgf_root_properties;

//Game Info Properties
typedef struct sgf_game_info_properties
{
	char* AN;	//Provides the name of the person, who made the annotations to the game.
	char* BR;	//Provides the rank of the black player.
	char* BT;	//Provides the name of the black team, if game was part of a team - match(e.g.China - Japan Supermatch).
	char* CP;	//Any copyright information .
	char* DT;	//Provides the date when the game was played.
	char* EV;	//Provides the name of the event.
	char* GN;	//Provides a name for the game.
	char* GC;	//Provides some extra information about the following game.
	char* ON;	//Provides some information about the opening played
	char* OT;	//Describes the method used for overtime (byo-yomi).
	char* PB;	//Provides the name of the black player.
	char* PC;	//Provides the place where the games was played.
	char* PW;	//Provides the name of the white player.
	char* RE;	//Provides the result of the game. 
	char* RO;	//Provides round-number and type of round.
	char* RU;	//Provides the used rules for this game.
	char* SO;	//Provides the name of the source(e.g.book, journal, ...).
	char* TM;	//Provides the time limits of the game.
	char* US;	//Provides the name of the user (or program), who entered the game.
	char* WR;	//Provides the rank of the white player. 
	char* WT;	//Provide the name of the white team.
}s_sgf_game_info_properties;

typedef struct sgf_miscellaneous_properties
{
	short FG;	//The figure property is used to divide a game into different figures for printing: a new figure starts at the node containing a figure property.
	char  PM;	//This property is used for printing.
	char* VW;	//View only part of the board.
}s_sgf_miscellaneous_properties;

typedef struct sgf_timing_properties
{
	char BL;	//Time left for black, after the move was made.
	char OB;	//Number of black moves left (after the move of this node was played) to play in this byo - yomi period.
	char OW;	//Number of white moves left (after the move of this node was played) to play in this byo - yomi period.
	char WL;	//Time left for white after the move was made.
}s_sgf_timing_properties;

//Node annotation properties

typedef struct sgf_node_annotation_properties
{
	char* C;	//Provides a comment text for the given node. 
	double DM;	//The position is even.
	double GB;	//Something good for black.
	double GW;	//Something good for white.
	double HO;	//Node is a 'hotspot', i.e. something interesting
	double N;	//Provides a name for the node. For more info have a look at the C - property.
	char* UC;	//The position is unclear. SGF viewers should display a message.This property must not be mixed with DM, GB or GW within a node.
	short V;	//Define a value for the node. 
}s_sgf_node_annotation_properties;

