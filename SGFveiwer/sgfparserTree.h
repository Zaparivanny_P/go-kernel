#ifndef SGF_PARSER_TREE
#define SGF_PARSER_TREE
#include <stdbool.h>

typedef struct sgfparser_track
{
	int *track;
	int lentgh;
}sgfparser_track;

typedef struct sgfparser_move
{
	char x;
	char y;
	char typeStone;					// 0 - black, 1 - white, ...
}sgfparser_move;

typedef struct sgfparser_branch
{
	sgfparser_move* moves;
	unsigned int lentgh_move;
	unsigned int iterator;
	short name_node;
	short connect_in;
	short* connect_out;
	short size_node;
	//short membership_of_node;
	char typeBranch;				// 0 - node, 1- end
	//	s_sgfparser_data_branch* branch;	//array branch

}sgfparser_branch;

typedef struct sgfparser_tree
{
	int size;
	short size_node;
	sgfparser_branch * sgf_branchs;
	int iterator;
	int index_branch;
}sgfparser_tree;


void sgfparsePrintBranch(sgfparser_branch* sgf_branch);

bool sgfparseTreeConstructor(sgfparser_tree* str, int size);
void sgfparseTreeDestructor(sgfparser_tree* str);
void sgfparseTreeGetTrack(sgfparser_tree* str_tree, sgfparser_track* track, int indexBranch);
int sgfparseTreeCreateConnection(sgfparser_tree* str_tree);
#endif
