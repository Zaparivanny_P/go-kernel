#ifndef STACK_H
#define STACK_H
#include <stdbool.h>

typedef struct stack
{
	unsigned int* stack_x;
	unsigned int* stack_y;
	int Iterator;
	int size;
}stack;

bool init_stack(stack *str, unsigned int size);
void deinit_stack(stack *str);
bool stack_pop(stack *str, unsigned int *x, unsigned int *y);
bool stack_push(stack *str, unsigned int x, unsigned int y);
void emptyStack(stack *str);

#endif STACK_H