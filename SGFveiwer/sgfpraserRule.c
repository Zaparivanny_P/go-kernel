#include "sgfpraserRule.h"
#include <stdio.h>
#include <stdlib.h>


#include "stack.h"

sgfparseBoard _sgfparse_board;
stack sgf_stack;
stack sgf_stack2;

int sgfparserMacroKilledGroup(unsigned short *x, unsigned short *y, int * sum_stone, sgf_killinfo *info_move);
int sgfparseGetDameStone(unsigned short x, unsigned short y);
int sgfparseBoardRevert(stack *sgf_stack, char color);

int sgfparserRuleConstructor(unsigned char board_size)
{
	int i, j;
	_sgfparse_board.size_board = board_size;
	_sgfparse_board.board = malloc(board_size * sizeof(int));
	if (_sgfparse_board.board == NULL)
		return -1;
	for (i = 0; i < board_size; i++)
	{
		_sgfparse_board.board[i] = malloc(board_size * sizeof(int));
		if (_sgfparse_board.board[i] == NULL)
		{
			sgfparserRuleDectructor();
			return -1;
		}
	}

	for (i = 0; i < _sgfparse_board.size_board; i++)
	{
		for (j = 0; j < _sgfparse_board.size_board; j++)
		{
			_sgfparse_board.board[i][j] = '+';
		}
	}

	if (!init_stack(&sgf_stack, _sgfparse_board.size_board * _sgfparse_board.size_board))
		return -1;
	if (!init_stack(&sgf_stack2, _sgfparse_board.size_board * _sgfparse_board.size_board))
		return -1;

	return 0;
}

int sgfparserRuleDectructor()
{
	int i;
	for (i = 0; i < _sgfparse_board.size_board; i++)
	{
		if (_sgfparse_board.board[i] != NULL)
		{
			free(_sgfparse_board.board[i]);
			_sgfparse_board.board[i] = NULL;
		}
			
	}
	if (_sgfparse_board.board != NULL)
	{
		free(_sgfparse_board.board);
		_sgfparse_board.board = NULL;
	}

	deinit_stack(&sgf_stack);
	deinit_stack(&sgf_stack2);
	return 0;
}

void sgfparserPrintBoard()
{
	int i, j;
	printf(" ");
	for (i = 0; i < _sgfparse_board.size_board; i++)
	{
		for (j = 0; j < _sgfparse_board.size_board; j++)
		{
			printf("%c ", _sgfparse_board.board[i][j]);
		}
		printf("\n ");
	}
}

int** sgfparserGetBoard()
{
	sgfparserPrintBoard();
	return _sgfparse_board.board;
}

bool sgfparseCheckAcsessMove(unsigned short x, unsigned short y)
{
	if (_sgfparse_board.board[x][y] == '+')
	{
		return true;
	}
	return false;	//TODO dont work
}

bool sgfparseCheckMoveInBoard(unsigned short x, unsigned short y)
{
	if ((x > (_sgfparse_board.size_board - 1)) || (y > (_sgfparse_board.size_board - 1)))
	{
		printf("Get out\n");
		return false;
	}
	
	return true;
}

int sgfparserKilledGroups(unsigned short x, unsigned short y)
{

	int sum_stone = 0;
	unsigned short tmp_x = 0, tmp_y = 0;
	static unsigned short ko_x, ko_y;
	sgf_killinfo info_move;

	if (_sgfparse_board.board[ko_x][ko_y] == '@')
	{
		_sgfparse_board.board[ko_x][ko_y] = '+';
	}

	x++;
	CHECKN(sgfparserMacroKilledGroup(&x, &y, &sum_stone, &info_move), ERROR_MALLOC);
	if (sum_stone == 1 && info_move.dame == 0) { tmp_x = x; tmp_y = y; }

	x -= 2;
	CHECKN(sgfparserMacroKilledGroup(&x, &y, &sum_stone, &info_move), ERROR_MALLOC);
	if (sum_stone == 1 && info_move.dame == 0) { tmp_x = x; tmp_y = y; }

	x++; y++;
	CHECKN(sgfparserMacroKilledGroup(&x, &y, &sum_stone, &info_move), ERROR_MALLOC);
	if (sum_stone == 1 && info_move.dame == 0) { tmp_x = x; tmp_y = y; }

	y -= 2;
	CHECKN(sgfparserMacroKilledGroup(&x, &y, &sum_stone, &info_move), ERROR_MALLOC);
	if (sum_stone == 1 && info_move.dame == 0) { tmp_x = x; tmp_y = y; }

	y++;
	int dame_stone = sgfparseGetDameStone(x, y);
	sgfparseBoardRevert(&sgf_stack, '+');
	printf("kill stone %i, dame %i\n", sum_stone, dame_stone);
	if (sum_stone == 1 && dame_stone == 1)
	{
		_sgfparse_board.board[tmp_x][tmp_y] = '@';
		ko_x = tmp_x;
		ko_y = tmp_y;
	}
	
	
	return 0;
}

ERROR_MOVE sgfparserAddMove(unsigned short x, unsigned short y, typeStone type)
{
	CHECK(sgfparseCheckAcsessMove(x, y), ERROR_STONE_EXIST);
	
	if (_sgfparse_board.board[x][y] != '+')
	{
		return ERROR_STONE_EXIST;
	}
	_sgfparse_board.board[x][y] = type;
	return 0;
}

ERROR_MOVE sgfparserHardAddMove(unsigned short x, unsigned short y, typeStone type)
{
	CHECK(sgfparseCheckMoveInBoard(x, y), ERROR_INPOSIBLE_MOVE);
	CHECK(sgfparseCheckAcsessMove(x, y), ERROR_STONE_EXIST);

	
	_sgfparse_board.board[x][y] = type;
	return 0;
}

ERROR_MOVE sgfparserAddNextMove(unsigned short x, unsigned short y)
{
	static typeStone type = STONE_BLACK;
	CHECK(sgfparseCheckMoveInBoard(x, y), ERROR_INPOSIBLE_MOVE);
	CHECK(sgfparseCheckAcsessMove(x, y), ERROR_STONE_EXIST);

	_sgfparse_board.board[x][y] = type;

	if (sgfparserKilledGroups(x, y))
	{
		printf("[WARING]\n");
	}

	
	if (type == STONE_BLACK)
		type = STONE_WHITE;
	else
		type = STONE_BLACK;
	return 0;
}

int sgfparseBoardRevert(stack *sgf_stack, char color)
{
	unsigned int x, y;
	while (stack_pop(sgf_stack, &x, &y))
	{
		_sgfparse_board.board[x][y] = color;
		//printf("set color '+' x %u, y %u\n", x, y);
	}
	return 0;
}

int sgfparseGetDameStone(unsigned short x, unsigned short y)
{
	int dame = 0;
	--x;
	if (x < _sgfparse_board.size_board && y < _sgfparse_board.size_board)
	{
		if (_sgfparse_board.board[x][y] == '+')
		{
			dame++;
			_sgfparse_board.board[x][y] = '-';
			stack_push(&sgf_stack, x, y);
		}
	}
	x+=2;

	if (x < _sgfparse_board.size_board && y < _sgfparse_board.size_board)
	if (_sgfparse_board.board[x][y] == '+')
	{
		dame++;
		_sgfparse_board.board[x][y] = '-';
		stack_push(&sgf_stack, x, y);
	}
	
	x--; y--;
	if (x < _sgfparse_board.size_board && y < _sgfparse_board.size_board)
	if (_sgfparse_board.board[x][y] == '+')
	{
		dame++;
		_sgfparse_board.board[x][y] = '-';
		stack_push(&sgf_stack, x, y);
	}

	y += 2;
	if (x < _sgfparse_board.size_board && y < _sgfparse_board.size_board)
	if (_sgfparse_board.board[x][y] == '+')
	{
		dame++;
		_sgfparse_board.board[x][y] = '-';
		stack_push(&sgf_stack, x, y);
	}
	
	return dame;
}


int floodFill4(unsigned short x, unsigned short y, char newColor, char oldColor, sgf_killinfo *info_move)
{
	CHECK(sgfparseCheckMoveInBoard(x, y), ERROR_INPOSIBLE_MOVE);

	if (_sgfparse_board.board[x][y] == oldColor)
	{

		_sgfparse_board.board[x][y] = newColor; //set color before starting recursion
		if (info_move != NULL)
		{
			stack_push(&sgf_stack2, x, y);
			info_move->dame += sgfparseGetDameStone(x, y);
			info_move->stone++;
		}
		floodFill4(x + 1, y, newColor, oldColor, info_move);
		floodFill4(x - 1, y, newColor, oldColor, info_move);
		floodFill4(x, y + 1, newColor, oldColor, info_move);
		floodFill4(x, y - 1, newColor, oldColor, info_move);
	}
	
	return 0;
}



bool sgfparseGetDame(unsigned short x, unsigned short y, sgf_killinfo *info_move)
{
	char oldColor = _sgfparse_board.board[x][y];
	char newColor;
	int dame = 0;


	switch (oldColor)
	{
	case 'x':
		newColor = 'X';
		break;
	case 'X':
		newColor = 'x';
		break;
	case 'O':
		newColor = 'o';
		break;
	case 'o':
		newColor = 'O';
		break;
	case '+':
		//printf("Stone no found\n");
		return -1;
	default:
		printf("Error color\n");
		return -1;
	}
	//printf("new color [%c], old color [%c]\n", newColor, oldColor);
	info_move->dame = 0;
	info_move->stone = 0;
	floodFill4(x, y, newColor, oldColor, info_move);
	sgfparseBoardRevert(&sgf_stack2, oldColor);
	sgfparseBoardRevert(&sgf_stack, '+');

	return true;
}


void sgfparseKillGroup(unsigned short x, unsigned short y)
{
	char oldColor = _sgfparse_board.board[x][y];
	if (oldColor == '+')
	{
		printf("Error color %c\n", oldColor);
		return;
	}
	char newColor = '+';
	printf("new color [%c], old color [%c]\n", newColor, oldColor);
	floodFill4(x, y, newColor, oldColor, NULL);
}

int sgfparserMacroKilledGroup(unsigned short *x, unsigned short *y, int * sum_stone, sgf_killinfo *info_move)
{
	CHECK(sgfparseCheckMoveInBoard(*x, *y), ERROR_INPOSIBLE_MOVE);
	char oldColor = _sgfparse_board.board[*x][*y];
	if (oldColor == '+')
	{
		//printf("no valid color\n");
		return -1;
	}

	
	info_move->dame = 0;
	info_move->stone = 0;
	CHECK(sgfparseGetDame(*x, *y, info_move), ERROR_MALLOC);
	printf("dame [%c] = %i, x %i, y %i\n", oldColor, info_move->dame, *x, *y);
	if (info_move->dame == 0)
	{
		*sum_stone += info_move->stone;
		sgfparseKillGroup(*x, *y);
	}
	return 0;
}

